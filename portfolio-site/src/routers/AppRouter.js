import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from '../components/HomePage';
import Header from '../components/Header';
import HelpPage from '../components/HelpPage';
import Portfolio from '../components/Portfolio';
import PortfolioPage from '../components/PortfolioPage';
import Contact from '../components/Contact';
import PageNotFound from '../components/PageNotFound';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header/>
      <Switch>
      <Route path="/" component={HomePage} exact={true} />
      <Route path="/portfolio" component={Portfolio} exact={true} />
      <Route path="/portfolio/:id" component={PortfolioPage} />
      <Route path="/contact" component={Contact} />
      <Route path="/help" component={HelpPage} />
      <Route component={PageNotFound} />
    </Switch>
    </div>
  </BrowserRouter>
)

export default AppRouter;
