import React from 'react';
import { NavLink } from 'react-router-dom';
const Header = () => (
  <header>
    <div>
      <h1>
        Portfolio
      </h1>
      <NavLink to="/" activeClassName="is-active" exact={true}>Home</NavLink>
      <NavLink to="/portfolio" activeClassName="is-active" exact={true}>Portfolio</NavLink>
      <NavLink to="/contact" activeClassName="is-active">Contact</NavLink>
      <NavLink to="/help" activeClassName="is-active">Help</NavLink>
    </div>
  </header>
)

export default Header;