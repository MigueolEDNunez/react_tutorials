var nameVar = 'Mike';
var nameVar = 'Nunez'
console.log('nameVar', nameVar);

let nameLet = 'Jet';
nameLet = 'Pet';
console.log("nameLet", nameLet);

const nameConst = 'Eduardo';

function getPetName() {
  const petName = 'Hal';
  return petName;
}

getPetName();
const petName = getPetName();
console.log(petName);


// Block scoping

var fullName = 'Mike Nunez';
let firstName;

if(fullName) {
  firstName = fullName.split(' ')[0];
  console.log(firstName);
}

console.log(firstName);
