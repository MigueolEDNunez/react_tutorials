class BuildItVisible extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
    this.state = {
      visible: false,
      value: "Hello"
    };
  }

  handleToggleVisibility() {
    this.setState((prevState) => {
      return {
        visible: !prevState.visible,
      }
    });
  }

  render() {
    return (
      <div>
        <button onClick={this.handleToggleVisibility}>{this.state.visible? 'Hide Details' : 'Show Details'}</button>
        <p>{this.state.visible? 'Hey. These are some details you can now see' : ''}</p>
      </div>
    )
  }
}

ReactDOM.render(<BuildItVisible/>, document.getElementById("app"));


// console.log("App is running");

// const page = {
//   title: 'Visibility Toggle'
// }

// let visibility = false;

// const onShow = () => {
//   visibility = !visibility;
//   renderTemplate();
// };

// const renderTemplate = () => {
//   const template = (
//     <div>
//       <h1>
//         {page.title}
//       </h1>
//       <button onClick={onShow}>
//         {visibility? 'Hide details' : 'Show details'}
//       </button>
//       {visibility && (<p>Hey. These are some details you can now see</p>)}
//     </div>
//   );
//   ReactDOM.render(template, appRoot);
// }

// const appRoot = document.getElementById("app");
// renderTemplate();
