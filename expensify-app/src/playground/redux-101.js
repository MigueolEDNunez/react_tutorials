// redux.js.org

import React from 'react';
import { createStore } from 'redux';

// Action generators - functions that return action objects

/* const add = ({ a, b }, c) => {
  return a + b;
}

console.log(add({ a: 1, b: 12 }, 100));*/

/* const incrementCount = () => {
  return {
    type: 'INCREMENT'
  };
};*/

const incrementCount = ({ incrementBy = 1 } = {}) => ({
  type: 'INCREMENT',
  incrementBy
});

const decrementCount = ({ decrementBy = 1 } = {}) => ({
  type: 'DECREMENT',
  decrementBy
});

const setCount = ({ count = 101 } = {}) => ({
  type: 'SET',
  count
});

const resetCount = () => ({
  type: 'RESET'
});

// Reducers
// 1. Reducers are pura functions
// 2. Never change state or action

let a = 10;
const add = (b) => {
  return a + b;
}

const countReducer = (state = { count: 0 }, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return {
        count: state.count + action.incrementBy
      }
    case 'DECREMENT':
      return {
        count: state.count - action.decrementBy
      }
    case 'RESET':
      return {
        count: 0
      }
    case 'SET':
      return {
        count: action.count
      }
    default:
      return state;
  }
};

const store = createStore(countReducer);

store.subscribe(() => {
  console.log(store.getState());
});

const unsubscribe = store.subscribe(() => {
  console.log(store.getState());
});

store.dispatch ({
  type: 'INCREMENT',
  incrementBy: 4
});


store.dispatch (decrementCount());

store.dispatch (decrementCount({ decrementBy: 10 }));

store.dispatch (setCount({ count: 101 }));

store.dispatch (resetCount())


/* store.dispatch ({
    type: 'RESET'
}); */

/* store.dispatch({
  type: 'SET',
  count: 101
}); */