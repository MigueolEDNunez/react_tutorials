/* console.log('destructuring');

// 
// Object destructuring
//

const person = {
  name: 'Miguel',
  age: '28',
  location: {
    city: 'Faro',
    temperature: 19
  }
};

const { name, age } = person;

console.log(`${person.name} is ${person.age} years old.`);
// or
console.log(`${name} is ${age} years old.`)

const { city, temperature: temp } = person.location;
if (city && temp) {
  console.log(`It's ${temp} in ${city}.`)
} */


// const book = {
//   title: 'Ego is the Enemy',
//   autor: 'Ryan Holiday',
//   publisher: {
//     name: 'Penguin'
//   }
// };

// const { title, autor } = book;
// const { name: publisherName = 'Self-Published' } = book.publisher;

// console.log(publisherName); // Self-Published

// 
// Array destructuring
// 

const address = ['1299 S Juniper Street', 'Philadelphia', 'Pennsylvania', '19147'];

const [street, city, state, zip] = address;

console.log(`You are in ${city} ${street}.`);


const item = ['Coffee', '2€00', '2€50', '2€80'];
const [coffeeName, smallCoffee, mediumCoffee, largeCoffee] = item;
console.log(`A medium ${coffeeName} costs ${mediumCoffee}`);